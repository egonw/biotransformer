/**
 * This class implements structure-based chemical categories, defined by a set of SMARTS 
 * patterns and/or boolean expressions.
 * 
 * @author Djoumbou Feunang, Yannick, PhD
 *
 */

package biotransformer.biomolecule;

public class StructuralChemicalCategory {
	
	public enum SCCategoryName {
		FATTY_ACID, UNSATURATED_FATTY_ACID, GLYCEROPHOSPHOLIPID, GLYCEROLIPID, SPHINGOLIPID,
		PRIMARY_BILE_ACID, SECONDARY_BILE_ACID, ALPHA_AMINO_ACID, GLYCEROL_3_PHOSPHATE_INOSITOL, C23_BILE_ACID, C24_BILE_ACID
		
		
	}

	public StructuralChemicalCategory() {
		// TODO Auto-generated constructor stub
	}

}
